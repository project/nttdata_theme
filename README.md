# NTT DATA Drupal theme

Drupal 11 component-based theme.

----

## Introduction

NTT DATA theme is built on the principles of Atomic Design methodology by Brad Frost, 
structuring components into atoms, molecules, organisms, templates, and pages.
This approach ensures a scalable and maintainable design system, enhancing the 
development process with reusable components.

Key features of the NTT DATA theme include:

Accessibility and Usability: Committed to inclusive solutions, the theme ensures compliance with accessibility guidelines, 
offering an intuitive, responsive interface with clear user feedback.

Single Directory Components (SDC): Used to create components by grouping all necessary files (Twig, YAML, CSS, JS) in a single directory, 
simplifying management, enhancing maintainability, and integrating seamlessly with Atomic Design principles.

SDC Display: A complementary module that allows you to configure which component an individual field uses or determine 
the component for a given view mode, providing flexible and precise control over content presentation.
Performance Optimization: Includes minified files, caching, and asynchronous resource loading for optimal performance.

Reusability: Comes with a variety of reusable components, accelerating development while maintaining high accessibility standards.


## Installation

> [!IMPORTANT]
> For drupal 11 projects, since the theme depends on the sdc_display module which is not yet in the drupal 11 version, 
> you need to run a few commands and enable the patches before installing the theme.

Install lenient
You can find more information about lenient [here](https://chandan-singh.com/blog/how-use-composer-drupal-lenient-when-upgrading-project-drupal-9-drupal-10)
```
composer require mglaman/composer-drupal-lenient
```

Add modules that are not in drupal 11
```
composer config --merge --json extra.drupal-lenient.allowed-list '["drupal/sdc_display", "drupal/nomarkup", "drupal/cl_editorial", "drupal/sdc_tags", "drupal/nomarkup"]'
```

Install patches
```
composer require cweagans/composer-patches'
```

Copy the patch lines from the theme's composer.json to the project's composer.json.
Copy the patches folder inside the theme to the root of the project and install the patches.


## Configuration
The theme requires these modules: CKEditor 5, Block Content, Configuration Manager, Media, Media Library, Taxonomy, Single Directory Components, SDC Display, Field Group, Paragraphs, No Markup.


## Development

#### Build


---




